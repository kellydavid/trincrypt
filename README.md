TrinCrypt
=========

TrinCrypt is a social media encryption application. It encrypts public posts as part of a group. Only members of that group can decrypt the posts. To other users of the social media network, they appear as ciphertext.

## Setting up Trincrypt
- Install the requirements from the requirements.txt file
`pip install -r requirements.txt`

- Set up some local environment variables
```
	export DATABASE_URL='dburl'
	export TRINCRYPT_TWITTER_CONSUMER_KEY='con_key'
	export TRINCRYPT_TWITTER_CONSUMER_SECRET='con_secret'
	export TRINCRYPT_TWITTER_CALLBACK='http://127.0.0.1:8000/callback'
```