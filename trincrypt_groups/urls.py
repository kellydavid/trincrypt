from django.conf.urls import patterns, url
from trincrypt_groups import views

urlpatterns = patterns( '',
		url(r'^$', views.group, name='group'),
		url(r'^(?P<group_id>\d+)$', views.details, name='details'),
	)