from django.db import models
from trincrypt_users.models import User

class Group(models.Model):
	name = models.CharField(max_length=30)
	members = models.ManyToManyField(User, verbose_name='group members', related_name='group_members')
	admin = models.ForeignKey(User, verbose_name='admin user', related_name='admin_user')		# User who can admin the group
	secret_key = models.CharField(max_length=200)

	def __str__(self):
		return self.name