from django.shortcuts import render, get_object_or_404
from trincrypt_users.models import User
from trincrypt_groups.models import Group
from trincrypt_posts.models import Post

def group(request):
	user = get_object_or_404(User, pk=request.session['trincrypt_user_id'])
	groups = Group.objects.all()
	context = {'user':user, 'groups':groups}
	return render(request, 'trincrypt_groups/group.html', context)

def details(request, group_id):
	user = get_object_or_404(User, pk=request.session['trincrypt_user_id'])
	group = get_object_or_404(Group, pk=group_id)
	posts = Post.objects.get(group=group_id)
	context = {'user':user, 'group':group, 'posts':posts}
	return render(request, 'trincrypt_groups/details.html', context)
