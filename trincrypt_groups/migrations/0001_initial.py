# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trincrypt_users', '0002_auto_20150331_0424'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('secret_key', models.CharField(max_length=200)),
                ('admin', models.ForeignKey(related_name='admin_user', verbose_name=b'admin user', to='trincrypt_users.User')),
                ('members', models.ManyToManyField(related_name='group_members', verbose_name=b'group members', to='trincrypt_users.User')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
