from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseNotFound
from models import User
from utils import *
from twitter_auth.utils import *

def profile(request, trincrypt_user_id):
	user = get_object_or_404(User, pk=trincrypt_user_id)
	twitter_user = get_twitter_user_from_request(request)
	return render(request, 'trincrypt_users/profile.html', {'user':user, 'twitter_user':twitter_user})

def register(request):
	twitter_user = get_twitter_user_from_request(request)
	is_registered = is_trincrypt_user(twitter_user.id_str)
	user = None
	if is_registered is False:
		user = User(twitter_user.id_str, twitter_user.name, twitter_user.screen_name)
		user.save()
	else:
		user = User.objects.get(pk=twitter_user.id_str)
	return render(request, 'trincrypt_users/register.html', {'is_registered':is_registered, 'user':user, 'twitter_user':twitter_user})
