# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('trincrypt_users', '0003_auto_20150331_0425'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='date_joined',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 31, 4, 25, 7, 928595, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
