# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id_tw', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('screen_name', models.CharField(max_length=20)),
                ('date_joined', models.DateTimeField(default=datetime.datetime(2015, 3, 31, 4, 24, 32, 937087, tzinfo=utc))),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
