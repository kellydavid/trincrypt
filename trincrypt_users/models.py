from django.utils import timezone
from django.db import models

class User(models.Model):
	id_tw = models.CharField(max_length=20, primary_key=True)
	name = models.CharField(max_length=20)
	screen_name = models.CharField(max_length=20)
	date_joined = models.DateTimeField(default=timezone.now())

	def __str__(self):
		return self.id_tw