from django.conf.urls import patterns, url
from trincrypt_users import views

urlpatterns = patterns( '',
		# user profile /user/<user-id>
		url(r'^(?P<trincrypt_user_id>\d+)$', views.profile, name='profile'),
		url(r'^register/$', views.register, name='register'),
	)