from models import User

def is_trincrypt_user(user_id):
	"""
	Will return true if a user exists for the given user_id
	"""
	try:
		user = User.objects.get(pk=user_id)
		return True
	except User.DoesNotExist:
		return False