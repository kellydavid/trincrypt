from django.conf.urls import patterns, url
from trincrypt_posts import views

urlpatterns = patterns( '',
		url(r'^create/$', views.create, name='create'),
		url(r'^details/$', views.details, name='details'),
		url(r'^(?P<post_id>\d+)/$', views.display, name='display'),
	)