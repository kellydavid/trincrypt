from django.db import models
from trincrypt_groups.models import Group
from trincrypt_users.models import User

class Post(models.Model):
	# twitter id of post
	user = models.ForeignKey(User)
	group = models.ForeignKey(Group)
	status_id = models.CharField(max_length=30)
	message = models.CharField(max_length=200)			# encrypted message
	pub_date = models.DateTimeField('date published')