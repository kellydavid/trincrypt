from Crypto.Cipher import AES
import base64
import os
 
# the block size for the cipher object; must be 16, 24, or 32 for AES
BLOCK_SIZE = 16
 
# the character used for padding--with a block cipher such as AES, the value
# you encrypt must be a multiple of BLOCK_SIZE in length. This character is
# used to ensure that your value is always a multiple of BLOCK_SIZE
PADDING = '{'
 
# one-liner to sufficiently pad the text to be encrypted
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
 
# one-liners to encrypt/encode and decrypt/decode a string
# encrypt with AES, encode with base64
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)

def encrypt(message, secret):
	cipher = AES.new(secret)
	return EncodeAES(cipher, message)

def decrypt(message, secret):
	cipher = AES.new(secret)
	return DecodeAES(cipher, message)

def generate_secret():
	return os.urandom(BLOCK_SIZE)

def convert_secret_to_hex(secret):
	return base64.b64encode(secret)

def convert_hex_to_secret(hex_str):
	return base64.b64decode(hex_str)