from django.shortcuts import render, get_object_or_404
from trincrypt_users.models import User
from trincrypt_groups.models import Group
from django.utils import timezone
from django import forms
from twitter_auth.utils import *
from models import Post
import crypto

def create(request):
	user = get_object_or_404(User, pk=request.session['trincrypt_user_id'])
	groups = Group.objects.filter(members=user)
	return render(request, 'trincrypt_posts/create.html', {'groups':groups})

def details(request):
	user = get_object_or_404(User, pk=request.session['trincrypt_user_id'])
	groups = Group.objects.filter(members=user)
	if request.method == 'POST':
		message = request.POST['message']
		try:
			group = Group.objects.get(id=request.POST['group'])
		except (KeyError, Group.DoesNotExist):
			return render(request, 'trincrypt_posts/create.html', {'groups':groups})
		# encrypt message
		secret_message = crypto.encrypt(message, crypto.convert_hex_to_secret(group.secret_key))
		# post tweet
		status = create_status(request, secret_message)
		#save post
		post = Post(user=user, status_id=status.id_str, group=group, message=secret_message, pub_date=timezone.now())
		post.save()
		context = {'post':post, 'group':group, 'message':message}
		return render(request, 'trincrypt_posts/details.html', context)
	else:
		return render(request, 'trincrypt_posts/create.html', {'groups':groups})

def display(request, post_id):
	user = get_object_or_404(User, pk=request.session['trincrypt_user_id'])
	post = get_object_or_404(Post, status_id=post_id)
	group = get_object_or_404(Group, id=post.group)
	secret_message = post.message
	message = decrypt(secret_message, convert_hex_to_secret(group.secret_key))
	context = {'user':user, 'post':post, 'group':group, 'message':message}
	return render(request, 'trincrypt_posts/display.html', context)

class CreatePostForm(forms.Form):
	message = forms.CharField(label='Message', max_length=95, widget=forms.Textarea)
	groups = forms.ChoiceField(label='Group')