# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trincrypt_users', '__first__'),
        ('trincrypt_groups', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status_id', models.CharField(max_length=30)),
                ('message', models.CharField(max_length=200)),
                ('pub_date', models.DateTimeField(verbose_name=b'date published')),
                ('group', models.ForeignKey(to='trincrypt_groups.Group')),
                ('user', models.ForeignKey(to='trincrypt_users.User')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
