import tweepy
from django.http import *

import os
# get the twitter consumer key and secret from env variables
CONSUMER_KEY = os.environ['TRINCRYPT_TWITTER_CONSUMER_KEY']
CONSUMER_SECRET = os.environ['TRINCRYPT_TWITTER_CONSUMER_SECRET']
CALLBACK = os.environ['TRINCRYPT_TWITTER_CALLBACK']

def get_oauth_from_request(request):
	auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
	auth.set_access_token(request.session.get('access_key_tw'), request.session.get('access_secret_tw'))
	return auth

def get_tweepy_api_from_request(request):
	return tweepy.API(get_oauth_from_request(request))

def get_twitter_user_from_request(request):
	return get_tweepy_api_from_request(request).me()

def create_status(request, message):
	api = get_tweepy_api_from_request(request)
	return api.update_status(status=message)

def set_trincrypt_user_id(request):
	request.session['trincrypt_user_id'] = get_twitter_user_from_request(request).id_str