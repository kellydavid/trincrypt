from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'trincrypt.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^user/', include('trincrypt_users.urls', namespace="trincrypt_users")),
    url(r'^group/', include('trincrypt_groups.urls', namespace="trincrypt_groups")),
    url(r'^post/', include('trincrypt_posts.urls', namespace="trincrypt_posts")),
    url(r'^', include('twitter_auth.urls')),
)
